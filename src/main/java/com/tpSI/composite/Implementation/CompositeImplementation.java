package com.tpSI.composite.Implementation;

import com.tpSI.composite.Entities.Etudiant;
import com.tpSI.composite.Entities.EtudiantInscrit;
import com.tpSI.composite.Entities.Inscription;
import com.tpSI.composite.Service.CompositeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.util.*;
import java.util.stream.Collectors;

@Service
public class CompositeImplementation implements CompositeService {
    private final RestTemplate restTemplate;

    public String urlE = "http://localhost:8771/getudiants/etudiant/list";
    public String urlIns = "http://localhost:8771/ginscriptions/inscription/list";

    @Autowired
    public CompositeImplementation(RestTemplate restTemplate) {
        this.restTemplate = restTemplate;
    }

    @Override
    public List<Inscription> getEtudiantsInscrits() {
        String inscriptionServiceUrl = "http://localhost:8771/ginscriptions/inscription/list";
        Inscription[] etudiantsArray = restTemplate.getForObject(inscriptionServiceUrl, Inscription[].class);

        return etudiantsArray != null ? Arrays.asList(etudiantsArray) : null;
    }

    @Override
    public List<Etudiant> getAllEtudiants() {
        String url = "http://localhost:8771/getudiants/etudiant/list";
        ResponseEntity<Etudiant[]> responseEntity = restTemplate.exchange(url, HttpMethod.GET, null, Etudiant[].class);
        Etudiant[] etudiantsArray = responseEntity.getBody();

        return etudiantsArray != null ? Arrays.asList(etudiantsArray) : null;
    }
    @Override
    public List<EtudiantInscrit> listEtudiant() {
        List<Etudiant> etudiant = restTemplate.getForObject(urlE, List.class);
        List<Inscription> inscription = restTemplate.getForObject(urlIns, List.class);

        System.out.println(etudiant);
//        return innerJoin(etudiant, inscription, "codeCandidat");
        return innerJoinByCodeCandiature(inscription,etudiant);
    }

    @Override
    public List<EtudiantInscrit> innerJoinByCodeCandiature(List<Inscription> inscrits, List<Etudiant> tousEtudiants) {
        Map<String, Inscription> inscritsMap = inscrits.stream()
                .collect(Collectors.toMap(Inscription::getCodeCandidat, inscrit -> inscrit));

        return tousEtudiants.stream()
                .filter(tousEtudiant -> inscritsMap.containsKey(tousEtudiant.getCodeCandidat()))
                .map(etudiantEtudDTO -> {
                    Inscription inscrit = inscritsMap.get(etudiantEtudDTO.getCodeCandidat());
                    return new EtudiantInscrit(inscrit.getCode(), inscrit.getCodeCandidat(), etudiantEtudDTO.getNom(), etudiantEtudDTO.getPrenom(),
                            etudiantEtudDTO.getEmail(), etudiantEtudDTO.getDateNaissance(), etudiantEtudDTO.getGenre(),
                            etudiantEtudDTO.getFiliere(), etudiantEtudDTO.getNiveau(),inscrit.getPaye(),inscrit.getMatricule());
                })
                .collect(Collectors.toList());
    }

//    public static List<JSONObject> innerJoin(List<JSONObject> list1, List<JSONObject> list2, String key) {
//        List<JSONObject> result = new ArrayList<>();
//        Map<String, JSONObject> map2 = new HashMap<>();
//
//        // Indexer les objets de la deuxième liste par leur clé
//        for (JSONObject obj2 : list2) {
//            map2.put(obj2.getString(key), obj2);
//        }
//
//        // Effectuer l'inner join
//        for (JSONObject obj1 : list1) {
//            if (map2.containsKey(obj1.getString(key))) {
//                JSONObject obj2 = map2.get(obj1.getString(key));
//                // Les objets ont la même clé, effectuer l'inner join
//                JSONObject joinedObj = new JSONObject();
//                joinedObj.put("codeCandidat", obj1.getString(key));
//                joinedObj.put("nom", obj1.getString("nom"));
//                joinedObj.put("prenom", obj1.getString("prenom"));
//                joinedObj.put("email", obj1.getString("email"));
//                joinedObj.put("dateNaissance", obj1.getString("dateNaissance"));
//                joinedObj.put("genre", obj1.getString("genre"));
//                joinedObj.put("matricule", obj2.getInt("matricule"));
//                joinedObj.put("paye", obj2.getInt("paye"));
//                result.add(joinedObj);
//            }
//        }
//
//        return result;
//    }
}
