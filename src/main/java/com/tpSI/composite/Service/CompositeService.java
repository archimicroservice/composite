package com.tpSI.composite.Service;

import com.tpSI.composite.Entities.Etudiant;
import com.tpSI.composite.Entities.EtudiantInscrit;
import com.tpSI.composite.Entities.Inscription;
import org.json.JSONObject;

import java.util.List;

public interface CompositeService {

    List<Inscription> getEtudiantsInscrits();

    List<Etudiant> getAllEtudiants();

    List<EtudiantInscrit> listEtudiant();

    List<EtudiantInscrit> innerJoinByCodeCandiature(List<Inscription> inscrits, List<Etudiant> tousEtudiants);
}
