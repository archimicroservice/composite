package com.tpSI.composite.Entities;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;
@NoArgsConstructor
//@AllArgsConstructor
//@RequiredArgsConstructor
@Data
public class EtudiantInscrit {
    private long code;

    private String codeCandidat;

    private String nom;
    private String prenom ;
    private String email ;


    private Date dateNaissance;
    private String genre;

    private String matricule;
    private String filiere;
    private int niveau;


    private int paye;

    public EtudiantInscrit(long code, String codeCandidat, String nom, String prenom, String email, Date dateNaissance, String genre, String filiere, int niveau, int paye,String matricule) {
        this.code = code;
        this.codeCandidat = codeCandidat;
        this.nom = nom;
        this.prenom = prenom;
        this.email = email;
        this.dateNaissance = dateNaissance;
        this.genre = genre;
        this.filiere = filiere;
        this.niveau = niveau;
        this.paye = paye;
        this.matricule=matricule;
    }



}
