package com.tpSI.composite.Entities;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@NoArgsConstructor
@AllArgsConstructor
//@RequiredArgsConstructor
@Data
public class Inscription {

    private long code;

    private String codeCandidat;

    private String matricule;
    private String filiere;
    private int niveau;

    private int paye;

    private boolean supprimeInscription=false;
}
