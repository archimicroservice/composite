package com.tpSI.composite.Entities;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;

@NoArgsConstructor
@AllArgsConstructor
//@RequiredArgsConstructor
@Data
public class Etudiant {

    private long code;

    private String codeCandidat;

    private String nom;
    private String prenom ;
    private String email ;


    private Date dateNaissance;
    private String genre;

    private String filiere;
    private int niveau;

    private boolean supprimeEtudiant=false;}