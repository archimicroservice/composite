package com.tpSI.composite.Controller;

import com.tpSI.composite.Entities.Etudiant;
import com.tpSI.composite.Entities.EtudiantInscrit;
import com.tpSI.composite.Entities.Inscription;
import com.tpSI.composite.Service.CompositeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

import java.util.List;

@RestController
@RequestMapping("/composite")
@CrossOrigin("*")
public class CompositeController {
    @Autowired
    private CompositeService compositeService;
    private final RestTemplate restTemplate;
    @Autowired
    public CompositeController(RestTemplate restTemplate) {
        this.restTemplate = restTemplate;
    }

    @GetMapping("/noteList")
    public List getUserById(Long userId) {
        String url = "http://localhost:8014/note/list";
        return restTemplate.getForObject(url, List.class);
    }

    @GetMapping("/etudiant/inscrit")
    public List<EtudiantInscrit> etudiantInscritDetails() {
        List<Inscription> inscrits = compositeService.getEtudiantsInscrits();
        List<Etudiant> tousEtudiants = compositeService.getAllEtudiants();

        System.out.println(inscrits);
        System.out.println(tousEtudiants);

        return compositeService.innerJoinByCodeCandiature(inscrits, tousEtudiants);
    }
}
